import numpy as np
import random
import gudhi as gd
from pylab import *
from matplotlib import pyplot as plt
import scipy
import scipy.sparse
import sklearn
import plotly.express as px 
import sklearn.cluster
import json
import os
import seaborn as sns

def area_triangle(point1,point2,point3):
    a = np.linalg.norm(point1-point2)
    b = np.linalg.norm(point2-point3)
    c = np.linalg.norm(point3-point1)
    s = (a + b + c)/2
    area = np.sqrt(s*(s-a)*(s-b)*(s-c))
    return area

def read_off_file(filename):
    """
    Reads a .off file and returns a tuple of the vertices and faces.
    """
    with open(filename, 'r') as f:
        first_line = f.readline().strip()
        if first_line != 'OFF':
            raise ValueError('First line of OFF file should be OFF')
        n_verts, n_faces, _ = [int(s) for s in f.readline().strip().split(' ')]
        verts = np.array([[float(s) for s in f.readline().strip().split(' ')] for _ in range(n_verts)])
        faces = np.array([[int(s) for s in f.readline().strip().split(' ')][1:] for _ in range(n_faces)])
    return verts, faces

def sample_points(verts, faces, n_samples):
    """
    Samples points from the surface of a mesh given by vertices and faces.
    """
    areas = np.array([area_triangle(verts[face[0]], verts[face[1]], verts[face[2]]) for face in faces])
    probs = areas / np.sum(areas)
    face_indices = np.random.choice(np.arange(len(faces)), size=n_samples, p=probs)
    points = np.zeros((n_samples, 3))
    for i, face_index in enumerate(face_indices):
        face = faces[face_index]
        points[i] = np.random.multivariate_normal(mean=np.array([0, 0, 0]), cov=np.eye(3))
        while True:
            b = np.random.uniform(0, 1, size=2)
            if b[0] + b[1] <= 1:
                break
        points[i] = (1 - b[0] - b[1]) * verts[face[0]] + b[0] * verts[face[1]] + b[1] * verts[face[2]]
    return points

def annulus(n, prob=0.1,r1=1, r2=2, offset=(0, 0)):
    result = []
    while len(result) < n:
        point = 2 * r2 * rand(2) - r2
        if r1 < np.linalg.norm(point) < r2 or rand()<prob:
            result.append((point[1] + offset[1], point[0] + offset[0]))
    return result

def positify(vecs):
    pos_vecs = []
    for vec in vecs:
        if np.sum(vec) > 0:
            pos_vecs.append(vec)
        else:
            pos_vecs.append(-vec)
    return np.array(pos_vecs)

def filled_rectangle(density,x,y):
    num_samples=int(np.floor(density*x*y))
    samples = [x,y]*np.random.rand(num_samples, 2)
    return np.array(samples)

def num_k_simplices(simplicial_tree, k):
    if k > 0:
        n = len(list(simplicial_tree.get_skeleton(k))) - \
            len(list(simplicial_tree.get_skeleton(k-1)))
    else:
        n = simplicial_tree.num_vertices()
    return n

def get_simplices(simplicial_tree):
    maxdim = simplicial_tree.dimension()
    simplices = []
    for i in range(maxdim+1):
        simplices.append([])
    for simplextuple in simplicial_tree.get_simplices():
        simplex = simplextuple[0]
        simplices[len(simplex)-1].append(simplex)
    return simplices

def build_simplex_dict(simplicial_tree, simplices):
    maxdim = simplicial_tree.dimension()
    num_k_simplices_in_p = []
    simplexdict = []
    for i in range(maxdim+1):
        num = num_k_simplices(simplicial_tree, i)
        num_k_simplices_in_p.append(num)
        #print('Number of '+str(i)+'-simplices: '+str(num))
        simplexdict.append(dict(
            zip([str(simplex) for simplex in simplices[i]], range(num_k_simplices_in_p[i]))))
    return num_k_simplices_in_p, simplexdict


def extract_boundary_operators(simplices, simplexdict, num_k_simplices_in_p):
    maxdim = len(num_k_simplices_in_p)-1
    boundary_operators = []
    for k in range(maxdim):
        newmatrix = scipy.sparse.coo_matrix(
            (num_k_simplices_in_p[k], num_k_simplices_in_p[k+1]))
        coordi = []
        coordj = []
        entries = []
        for simplex in simplices[k+1]:
            simplex_index = simplexdict[k+1][str(simplex)]
            for i in range(k+2):
                new_simplex = simplex.copy()
                new_simplex.pop(i)
                new_simplex_index = simplexdict[k][str(new_simplex)]
                coordi.append(new_simplex_index)
                coordj.append(simplex_index)
                if i % 2 == 0:
                    entries.append(1)
                else:
                    entries.append(-1)
        boundary_operators.append(scipy.sparse.csc_matrix((np.array(entries), (np.array(coordi), np.array(
            coordj))), shape=(num_k_simplices_in_p[k], num_k_simplices_in_p[k+1]), dtype=float))
    return boundary_operators

def degree(boundary_operators, k):
    B = np.abs(boundary_operators[k])
    degrees = np.sum(B, axis=1)
    return degrees


def Adjacency_Matrix(boundary_operators, k):
    Bk = boundary_operators[k]
    A = -Bk@Bk.transpose()+scipy.sparse.diags(np.squeeze(np.asarray(degree(boundary_operators, k))))
    return A


def Hodge_Laplacian(boundary_operators, k, rel = 1, id =0):
    if k == len(boundary_operators):
        Bkm = boundary_operators[k-1]
        A = Bkm.transpose()@Bkm
    elif k > 0:
        Bk = boundary_operators[k]
        Bkm = boundary_operators[k-1]
        A = rel*Bk@Bk.transpose()+(2-rel)*Bkm.transpose()@Bkm
    else:
        Bk = boundary_operators[k]
        A = Bk@Bk.transpose()
    A = A + id*scipy.sparse.identity(A.shape[0])
    return A

def Hodge_Laplacian_Up_Down(boundary_operators, k):
    if k == len(boundary_operators):
        Bkm = boundary_operators[k-1]
        A = Bkm.transpose()@Bkm
        Aup = np.zeros(A.shape)
        Adown = Bkm.transpose()@Bkm
    elif k > 0:
        Bk = boundary_operators[k]
        Bkm = boundary_operators[k-1]
        Adown = Bkm.transpose()@Bkm
        Aup = Bk@Bk.transpose()
        A = Bk@Bk.transpose()+Bkm.transpose()@Bkm
    else:
        Bk = boundary_operators[k]
        A = Bk@Bk.transpose()
        Aup = Bk@Bk.transpose()
        Adown = np.zeros(A.shape)
    return A, Aup, Adown

def transform_vector_upper(vec, simplices1, dict2, len2):
    vec2 = np.zeros(len2)
    for index in range(len(simplices1)):
        simplex = simplices1[index]
        simplex_index = dict2[str(simplex)]
        vec2[simplex_index] = vec[index]
    return vec2

def transform_vector_lower(vec, dict1, simplices2):
    vec2 = np.zeros(len(simplices2))
    for index in range(len(simplices2)):
        simplex = simplices2[index]
        simplex_index = dict1[str(simplex)]
        vec2[index] = vec[simplex_index]
    return vec2

def construct_simplices(points, length, type = 'alpha', dim = 1):
    max_edge_length_Barcodes = length
    if type == 'alpha':
        Test_Komplex = gd.AlphaComplex(
            points = points)
        Rips_simplex_tree_sample = Test_Komplex.create_simplex_tree(
            max_alpha_square=max_edge_length_Barcodes**2)
    elif type == 'rips':
        Test_Komplex = gd.RipsComplex(
            points = points, max_edge_length=max_edge_length_Barcodes)
        Rips_simplex_tree_sample = Test_Komplex.create_simplex_tree(
            )
    simplicial_tree = Rips_simplex_tree_sample
    simplices = get_simplices(simplicial_tree)
    num_k_simplices_in_p, simplexdict = build_simplex_dict(
        simplicial_tree=simplicial_tree, simplices=simplices)
    boundary_operators = extract_boundary_operators(
        simplices, simplexdict, num_k_simplices_in_p)
    for k in [dim]:
        Hnormal, Hup, Hdown = Hodge_Laplacian_Up_Down(
                boundary_operators=boundary_operators, k=k)
    cur_points = np.array([Test_Komplex.get_point(i) for i in range(len(points))])
    return Hnormal, Hup, Hdown, simplices, simplexdict, num_k_simplices_in_p, cur_points

colourBars = [matplotlib.cm.Blues, matplotlib.cm.Greens, matplotlib.cm.Reds, matplotlib.cm.Purples, matplotlib.cm.Greys, matplotlib.cm.Oranges, matplotlib.cm.YlOrBr, matplotlib.cm.YlOrRd, matplotlib.cm.OrRd, matplotlib.cm.PuRd, matplotlib.cm.RdPu, matplotlib.cm.BuPu, matplotlib.cm.GnBu, matplotlib.cm.PuBu, matplotlib.cm.YlGnBu, matplotlib.cm.PuBuGn, matplotlib.cm.BuGn, matplotlib.cm.YlGn]