# Disentangling the Spectral Properties of the Hodge Laplacian: Not All Small Eigenvalues Are Equal
## Accompanying code

This is the code accompanying the paper ["Disentangling the Spectral Properties of the Hodge Laplacian: Not All Small Eigenvalues Are Equal"](https://arxiv.org/abs/2311.14427), Vincent P. Grande and Michael T. Schaub, International Conference on Acoustics, Speech, and Signal Processing 2024.
In case you have any questions on the code or the paper, feel free to reach out!

### Installation
The Jupyter Notebook runs on Python 3.10 and using the requirements in requirements.txt.

### License
See the license file

### Funding
VPG acknowledges funding by the German Research Council (DFG) within Research Training Group 2236 (UnRAVeL).
MTS acknowledges partial funding by the Ministry of Culture and Science (MKW) of the German State of North Rhine Westphalia ("NRW Rückkehrprogramm") and the European Union (ERC, HIGH-HOPeS, 101039827). Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union or the European Research Council Executive Agency. Neither the European Union nor the granting authority can be held responsible for them.